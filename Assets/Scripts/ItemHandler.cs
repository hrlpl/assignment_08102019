﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHandler : MonoBehaviour
{
    [SerializeField] private Interactables.Item itemType;
    private RectTransform highlighterOne, highlighterTwo; // the background glow sprite
    private void Awake()
    {
        highlighterOne = GameManager.Instance.PlayerId[0].Highlighter;
        highlighterTwo = GameManager.Instance.PlayerId[1].Highlighter;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //set glow sprite to triggered item and notify current applicable action to player
        if (collision.tag == "PlayerOne")
        {
            

            highlighterOne.SetParent(transform);
            highlighterOne.sizeDelta = highlighterOne.anchoredPosition = Vector2.zero;
            highlighterOne.gameObject.SetActive(true);
            GameManager.Instance.PlayerId[0].PlayerAction = itemType;
        }
        else if (collision.tag == "PlayerTwo")
        {
            highlighterTwo.SetParent(transform);
            highlighterTwo.sizeDelta = highlighterTwo.anchoredPosition = Vector2.zero;
            highlighterTwo.gameObject.SetActive(true);
            GameManager.Instance.PlayerId[1].PlayerAction = itemType;
        }
            

        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        // remove glow and remove applicable action for triggered player
        if (collision.tag == "PlayerOne")
        {

            highlighterOne.gameObject.SetActive(false);
            GameManager.Instance.PlayerId[0].PlayerAction = Interactables.Item.NONE;
        }
        else if (collision.tag == "PlayerTwo")
        {
            highlighterTwo.gameObject.SetActive(false);
            GameManager.Instance.PlayerId[1].PlayerAction = Interactables.Item.NONE;
        }

    }
}
