﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using UnityEngine.UI;
using Pixelplacement.TweenSystem;

public class Customer : MonoBehaviour
{
    [SerializeField] private List<int> order;

    [SerializeField] private Image[] orderIcons;
    [SerializeField] private Image timer;



    private int waitTime;
    private int waitTimeMultiplier = 1;
    private bool isServed,servedWrongOne, servedWrongTwo;
    private bool isAngry = false;
    private TweenBase timerHandler;
    private string servedBy="";
    private float elasped = 0.0f;
    public List<int> Order { get => order; set => order = value; }
    public TweenBase TimerHandler { get => timerHandler; set => timerHandler = value; }
    public bool IsServed { get => isServed; set => isServed = value; }
    public bool ServedWrongTwo { get => servedWrongTwo; set => servedWrongTwo = value; }
    public bool ServedWrongOne { get => servedWrongOne; set => servedWrongOne = value; }
    public Image Timer { get => timer; set => timer = value; }
    public bool IsAngry { get => isAngry; set => isAngry = value; }
    public string ServedBy { get => servedBy; set => servedBy = value; }

    // Start is called before the first frame update
    void Awake()
    {
        Order = new List<int>();
    }


    // spawns the customer with values set
    public void SpawnCx()
    {
        IsServed = ServedWrongOne = ServedWrongTwo = false;
        Timer.color = Color.green;
        waitTimeMultiplier = 1;
        elasped = 0.0f;
        ServedBy = "";
        gameObject.SetActive(true);
        transform.localScale = Vector3.zero;

        Order.Clear();
        waitTime = 0;

        var numbers = new List<int>(6);
        for (int i = 1; i <= 6; i++)
        {
            numbers.Add(i);
        }

        for (int j = 0; j < Random.Range(2, 4); j++)
        {
            var thisNumber = Random.Range(0, numbers.Count);
            Order.Add(numbers[thisNumber]);
            numbers.RemoveAt(thisNumber);
        }

        for (int i = 0; i < Order.Count; i++)
        {
            var veg = GameManager.Instance.VeggiesList[Order[i] - 1];
            orderIcons[i].sprite = veg.VeggieIcon;
            waitTime += veg.CutTime;
            orderIcons[i].gameObject.SetActive(true);
        }

        waitTime += 60;

        
        Tween.LocalScale(transform, Vector3.zero, Vector3.one, 0.5f, 0f,completeCallback:StartTimer);

    }

    //starts the waiting period
    void StartTimer()
    {
        TimerHandler = Tween.Value(1.0f, 0.0f, (val) => { Timer.fillAmount = val; },waitTime, 0,completeCallback:DespawnCx);
 
    }

    //enteres into angry mode upon wrong food served
    public void AngryMode()
    {
        IsAngry = true;
        CalcElaspedTime();
        TimerHandler.Stop();
        
        TimerHandler = Tween.Value(Timer.fillAmount, 0.0f, (val) => { Timer.fillAmount = val; }, (waitTime - elasped)/2.0f, 0, completeCallback: DespawnCx);
        
    }
   
    //calculate the cumulative time waited
    public void CalcElaspedTime()
    {
        elasped += TimerHandler.Percentage * TimerHandler.Duration;
    }

    //upon served OR not served customer left
    public void DespawnCx()
    {
        if (!IsServed)
        {
            GameManager.Instance.PenalizeAll(this);
            if(ServedWrongOne)
                GameManager.Instance.Penalize(0,this);
            if(ServedWrongTwo)
                GameManager.Instance.Penalize(1,this);
        }
        else
        {
            if (elasped/waitTime <=0.7f)
            {
                GameManager.Instance.InitReward(ServedBy);
            }
        }

        GameManager.Instance.SpawnCxAgain(this);
        Tween.LocalScale(transform, Vector3.one, Vector3.zero, 0.25f, 0f, completeCallback:()=>{
            gameObject.SetActive(false);
        });
    }
}
