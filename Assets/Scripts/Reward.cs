﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Reward : MonoBehaviour
{
    [SerializeField] private Image rewardIcon;
    [SerializeField] private Sprite[] rewards; // available rewards
    private string rewardFor; // applicable for the player with this TAG
    private int rewardType; // Current reward type

    public string RewardFor { get => rewardFor; set => rewardFor = value; }

    public void init()
    {
        //set this reward type & position randomly on the available screen space
        rewardType = Random.Range(0, 3);
        rewardIcon.sprite = rewards[rewardType];
        var rectPos = GetComponentInParent<RectTransform>();
        transform.localPosition = new Vector3(
            Random.Range(-rectPos.sizeDelta.x / 2.0f, rectPos.sizeDelta.x / 2.0f),
            Random.Range(-rectPos.sizeDelta.y / 2.0f, rectPos.sizeDelta.y / 2.0f),
            0
            );
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if trigerred by correct player then award the reward
        if(collision.tag == RewardFor)
        {
            collision.GetComponent<Player>().RewardPlayer(rewardType);
            Destroy(gameObject);
        }
    }
}
