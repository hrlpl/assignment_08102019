﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Pixelplacement;

public class Player : MonoBehaviour
{
    [SerializeField] private Rigidbody2D playerBody;
    [SerializeField] private float moveSpeed;
    [SerializeField] private Image selectedVegOne;
    [SerializeField] private Image selectedVegTwo;
    [SerializeField] private GameObject waitIcon;
    [SerializeField] private TextMeshProUGUI timeLabel;
    [SerializeField] private TextMeshProUGUI scoreLabel;
    [SerializeField] private Transform trayHolder;
    [SerializeField] private int playerId;

    [SerializeField] private RectTransform highlighter;

    private int timeCount;
    private int scoreCount;
    private Vector2 playerDirection;

    private Queue<Veggie> selectedVeggies;
    private Queue<Veggie> trayVeggies;
    private List<int> trayVeggieIds;
    private bool playerCanMove = true;
    private bool isDead;
    private int speedMul = 0;
    

    
    private Interactables.Item playerAction = Interactables.Item.NONE;




    public Interactables.Item PlayerAction { get => playerAction; set => playerAction = value; }
    public RectTransform Highlighter { get => highlighter; set => highlighter = value; }
    public Queue<Veggie> SelectedVeggies { get => selectedVeggies; set => selectedVeggies = value; }
    public Queue<Veggie> TrayVeggies { get => trayVeggies; set => trayVeggies = value; }
    public List<int> TrayVeggieIds { get => trayVeggieIds; set => trayVeggieIds = value; }
    public bool PlayerCanMove { get => playerCanMove; set => playerCanMove = value; }
    public Image SelectedVegOne { get => selectedVegOne; set => selectedVegOne = value; }
    public Image SelectedVegTwo { get => selectedVegTwo; set => selectedVegTwo = value; }
    public GameObject WaitIcon { get => waitIcon; set => waitIcon = value; }
    public Transform TrayHolder { get => trayHolder; set => trayHolder = value; }
    public int ScoreCount { get => scoreCount; set => scoreCount = value; }
    public bool IsDead { get => isDead; set => isDead = value; }
    public int TimeCount { get => timeCount; set => timeCount = value; }

    private void Awake()
    {
        SelectedVeggies = new Queue<Veggie>();
        trayVeggieIds = new List<int>();
    }
    private void Start()
    {
        selectedVeggies.Clear();
        TrayVeggieIds.Clear();
        TrayVeggieIds.Clear();
        IsDead = false;
        playerCanMove = true;
        TimeCount = 60;
        ScoreCount = 0;
        StartCoroutine("LoseTime");
    }

    void Update()
    {
        if (PlayerCanMove)
        {
            playerDirection = Vector2.zero;
            if (playerId==0)
            {
                if (Input.GetKey(KeyCode.A)) playerDirection.x -= 1;
                if (Input.GetKey(KeyCode.D)) playerDirection.x += 1;
                if (Input.GetKey(KeyCode.W)) playerDirection.y += 1;
                if (Input.GetKey(KeyCode.S)) playerDirection.y -= 1;
                if (Input.GetKeyDown(KeyCode.LeftControl))
                    GameManager.Instance.Interact(playerId,PlayerAction);

                playerBody.velocity = playerDirection * moveSpeed;
            }
            else if (playerId ==1)
            {
                if (Input.GetKey(KeyCode.LeftArrow)) playerDirection.x -= 1;
                if (Input.GetKey(KeyCode.RightArrow)) playerDirection.x += 1;
                if (Input.GetKey(KeyCode.UpArrow)) playerDirection.y += 1;
                if (Input.GetKey(KeyCode.DownArrow)) playerDirection.y -= 1;
                if (Input.GetKeyDown(KeyCode.RightControl))
                    GameManager.Instance.Interact(playerId,PlayerAction);

                playerBody.velocity = playerDirection * moveSpeed;
            }
        }
        else
        {
            playerBody.velocity = Vector2.zero;
        }

    }

    IEnumerator LoseTime()
    {
        while (TimeCount>0)
        {
            yield return new WaitForSeconds(1);
            TimeCount--;
            timeLabel.text = TimeCount + "";
        }
        //player is dead
        PlayerCanMove = false;
        IsDead = true;
        GameManager.Instance.EndGame();
    }

    public void UpdateScore(int val)
    {
        scoreCount = val;
        if (scoreCount <= 0)
            scoreCount = 0;
        scoreLabel.text = scoreCount + "";
    }

    public void RewardPlayer(int type)
    {
        switch (type)
        {
            case 0:
                if (speedMul == 0)
                {
                    speedMul = 2;
                    Tween.Value(0, 1, (val) => { }, 20, 0, startCallback: () => { moveSpeed *= speedMul; }, completeCallback: () => { moveSpeed /= speedMul; speedMul = 0; });
                }
                break;
            case 1:
                UpdateScore(ScoreCount + 5);
                break;
            case 2:
                TimeCount += 5;
                timeLabel.text = TimeCount + "";
                break;
            default:
                break;
        }
    }
}
