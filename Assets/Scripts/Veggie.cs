﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Veggie : MonoBehaviour
{
    [SerializeField] private int veggieId;
    [SerializeField] private int cutTime;

    private Sprite veggieIcon;

    public Sprite VeggieIcon { get => veggieIcon; set => veggieIcon = value; }
    public int VeggieId { get => veggieId; set => veggieId = value; }
    public int CutTime { get => cutTime; set => cutTime = value; }

    private void Awake()
    {
        VeggieIcon = GetComponent<Image>().sprite;
    }

}
