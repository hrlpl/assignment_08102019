﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] private Player[] playerId;
    [SerializeField] private Transform rewardHolder;



    [Space(50)]
    [SerializeField] private Veggie[] veggies;
    [SerializeField] private Transform[] chopBoards;
    [SerializeField] private Customer[] customerList;
    [SerializeField] private Image[] plates;

    [Space(50)]
    [SerializeField] private Transform choppedVeggiePf;
    [SerializeField] private Transform rewardPf;

    [Space(50)]
    [SerializeField] private GameObject gameUI;
    [SerializeField] private GameObject deadUI;
    [SerializeField] private TMP_InputField winnerName;
    [SerializeField] private TextMeshProUGUI scoreList;

    private Veggie[] plateVeggies;
    private List<Queue<Veggie>> choppedVeggies;
    private List<List<int>> choppedVeggieIds;

    public Veggie[] VeggiesList { get => veggies; set => veggies = value; }

    public Image[] Plates { get => plates; set => plates = value; }
    public Veggie[] PlateVeggies { get => plateVeggies; set => plateVeggies = value; }
    public Transform[] ChopBoards { get => chopBoards; set => chopBoards = value; }
    public List<Queue<Veggie>> ChoppedVeggies { get => choppedVeggies; set => choppedVeggies = value; }
    public List<List<int>> ChoppedVeggieIds { get => choppedVeggieIds; set => choppedVeggieIds = value; }
    public Player[] PlayerId { get => playerId; set => playerId = value; }

    void Start()
    {


        ChoppedVeggies = new List<Queue<Veggie>>();
        ChoppedVeggieIds = new List<List<int>>();
        for (int i = 0; i < 2; i++)
        {

            ChoppedVeggies.Add(new Queue<Veggie>());
            ChoppedVeggieIds.Add(new List<int>());

        }

        PlateVeggies = new Veggie[2];
        foreach(Customer cx in customerList)
        {
            Tween.Value(0, 1, (val) => { }, Random.Range(2,15), 0,
                    completeCallback:cx.SpawnCx);
        }
    }



    public void Interact(int playerId, Interactables.Item playerAction)
    {
        switch (playerAction)
        {
            case Interactables.Item.NONE:
                break;

            case Interactables.Item.VEGGIE:
                InteractVeggie(playerId);
                break;

            case Interactables.Item.CHOPBOARD_L:
                InteractChopBoard(0, playerId);
                break;
            case Interactables.Item.CHOPBOARD_R:
                InteractChopBoard(1, playerId);
                break;

            case Interactables.Item.CUSTOMER:
                InteractCustomer(playerId);
                break;
            case Interactables.Item.BIN:
                InteractBin(playerId);
                break;
            case Interactables.Item.PLATE_L:
                InteractPlate(0, playerId);
                break;
            case Interactables.Item.PLATE_R:
                InteractPlate(1, playerId);
                break;
            default:
                break;
        }
    }

    void InteractVeggie(int playerId)
    {
        var curVeggie = PlayerId[playerId].Highlighter.parent.GetComponent<Veggie>();
        if (!PlayerId[playerId].SelectedVeggies.Contains(curVeggie))
        {
            if (!PlayerId[playerId].SelectedVegOne.gameObject.activeSelf)
            {

                PlayerId[playerId].SelectedVeggies.Enqueue(curVeggie);
                PlayerId[playerId].SelectedVegOne.sprite = curVeggie.VeggieIcon;
                PlayerId[playerId].SelectedVegOne.gameObject.SetActive(true);

            }
            else if (!PlayerId[playerId].SelectedVegTwo.gameObject.activeSelf)
            {
                PlayerId[playerId].SelectedVeggies.Enqueue(curVeggie);
                PlayerId[playerId].SelectedVegTwo.sprite = curVeggie.VeggieIcon;
                PlayerId[playerId].SelectedVegTwo.gameObject.SetActive(true);
            }
        }
    }

    void InteractChopBoard(int side, int playerId)
    {
        if (PlayerId[playerId].SelectedVeggies.Count > 0)
        {
            var veggie = PlayerId[playerId].SelectedVeggies.Dequeue();

            if (PlayerId[playerId].SelectedVegOne.gameObject.activeSelf)
            {
                PlayerId[playerId].SelectedVegOne.gameObject.SetActive(false);
            }
            else if (PlayerId[playerId].SelectedVegTwo.gameObject.activeSelf)
            {
                PlayerId[playerId].SelectedVegTwo.gameObject.SetActive(false);
            }

            if (!ChoppedVeggies[side].Contains(veggie))
            {
                ChoppedVeggies[side].Enqueue(veggie);
                ChoppedVeggieIds[side].Add(veggie.VeggieId);
                var go = Instantiate(choppedVeggiePf);
                go.SetParent(ChopBoards[side]);
                go.GetComponent<Image>().sprite = veggie.VeggieIcon;
                PlayerId[playerId].PlayerCanMove = false;
                Tween.Value(0, 1, (val) => { }, veggie.CutTime, 0, startCallback: () => { PlayerId[playerId].WaitIcon.SetActive(true); },
                    completeCallback: () => { PlayerId[playerId].PlayerCanMove = true; PlayerId[playerId].WaitIcon.SetActive(false); });
            }
        }
        else if (PlayerId[playerId].TrayHolder.childCount > 0)
        {
            while (PlayerId[playerId].TrayHolder.childCount > 0)
            {
                PlayerId[playerId].TrayHolder.GetChild(PlayerId[playerId].TrayHolder.childCount - 1).SetParent(ChopBoards[side]);
            }

            ChoppedVeggieIds[side] = new List<int>(PlayerId[playerId].TrayVeggieIds);
            PlayerId[playerId].TrayVeggieIds.Clear();

            ChoppedVeggies[side] = new Queue<Veggie>(PlayerId[playerId].TrayVeggies);
            PlayerId[playerId].TrayVeggies.Clear();


        }
        else if (ChoppedVeggies.Count > 0)
        {
            while (ChopBoards[side].childCount > 0)
            {
                ChopBoards[side].GetChild(ChopBoards[side].childCount - 1).SetParent(PlayerId[playerId].TrayHolder);
            }

            PlayerId[playerId].TrayVeggies = new Queue<Veggie>(ChoppedVeggies[side]);
            ChoppedVeggies[side].Clear();

            PlayerId[playerId].TrayVeggieIds = new List<int>(ChoppedVeggieIds[side]);
            ChoppedVeggieIds[side].Clear();
        }
    }

    void InteractCustomer(int playerId)
    {
        if ( PlayerId[playerId].TrayVeggieIds.Count > 0)
        {
            var customerHandle = PlayerId[playerId].Highlighter.parent.GetComponent<Customer>();


            bool isCorrectOrder = true;

            if (PlayerId[playerId].TrayVeggieIds.Count == (customerHandle.Order.Count))
            {
                foreach (int vegId in PlayerId[playerId].TrayVeggieIds)
                    if (!customerHandle.Order.Contains(vegId))
                    {
                        isCorrectOrder = false;
                        break;
                    }
            }
            else
            {
                isCorrectOrder = false;
            }

            if (isCorrectOrder)
            {
                PlayerId[playerId].TrayVeggieIds.Clear();
                PlayerId[playerId].TrayVeggies.Clear();
                customerHandle.IsServed = true;
                customerHandle.ServedBy = PlayerId[playerId].tag;
                customerHandle.CalcElaspedTime();
                customerHandle.TimerHandler.Finish();

                PlayerId[playerId].UpdateScore(
                    PlayerId[playerId].ScoreCount + customerHandle.Order.Count
                    );

                foreach (Transform tf in PlayerId[playerId].TrayHolder)
                    Destroy(tf.gameObject);

                Debug.Log("Correct Serve");
            }
            else
            {
                Debug.Log("Wrong Serve");

                if (playerId == 0)
                    customerHandle.ServedWrongOne = true;
                else if (playerId == 1)
                    customerHandle.ServedWrongTwo = true;
                customerHandle.Timer.color = Color.red;
                if (!customerHandle.IsAngry)
                    customerHandle.AngryMode();

                /*            foreach (int vegId in Player[playerId].TrayVeggieIds)
                                Debug.Log("Chopped Order " + vegId);

                            foreach (int vegId in customerHandle.Order)
                                Debug.Log("Placed Order " + vegId);*/
            }
        }
    }

    void InteractPlate(int side, int playerId)
    {
        if (PlayerId[playerId].SelectedVeggies.Count > 0 && !GameManager.Instance.Plates[side].gameObject.activeSelf)
        {
            var veggie = PlayerId[playerId].SelectedVeggies.Dequeue();

            if (PlayerId[playerId].SelectedVegOne.gameObject.activeSelf)
            {
                PlayerId[playerId].SelectedVegOne.gameObject.SetActive(false);
                GameManager.Instance.Plates[side].sprite = PlayerId[playerId].SelectedVegOne.sprite;
            }
            else if (PlayerId[playerId].SelectedVegTwo.gameObject.activeSelf)
            {
                PlayerId[playerId].SelectedVegTwo.gameObject.SetActive(false);
                GameManager.Instance.Plates[side].sprite = PlayerId[playerId].SelectedVegTwo.sprite;
            }

            GameManager.Instance.PlateVeggies[side] = veggie;
            GameManager.Instance.Plates[side].gameObject.SetActive(true);
        }
        else if (PlayerId[playerId].SelectedVeggies.Count <= 1 && GameManager.Instance.Plates[side].gameObject.activeSelf)
        {
            PlayerId[playerId].SelectedVeggies.Enqueue(GameManager.Instance.PlateVeggies[side]);

            if (!PlayerId[playerId].SelectedVegOne.gameObject.activeSelf)
            {
                PlayerId[playerId].SelectedVegOne.gameObject.SetActive(true);
                PlayerId[playerId].SelectedVegOne.sprite = GameManager.Instance.Plates[side].sprite;
            }
            else if (!PlayerId[playerId].SelectedVegTwo.gameObject.activeSelf)
            {
                PlayerId[playerId].SelectedVegTwo.gameObject.SetActive(true);
                PlayerId[playerId].SelectedVegTwo.sprite = GameManager.Instance.Plates[side].sprite;
            }
            GameManager.Instance.Plates[side].gameObject.SetActive(false);
            GameManager.Instance.PlateVeggies[side] = null;
        }
    }

    void InteractBin(int playerId)
    {
        var count = PlayerId[playerId].TrayHolder.childCount;
        if ( count> 0)
        {
            
            PlayerId[playerId].UpdateScore(PlayerId[playerId].ScoreCount - count);
            PlayerId[playerId].TrayVeggieIds.Clear();
            PlayerId[playerId].TrayVeggies.Clear();
            foreach (Transform tf in PlayerId[playerId].TrayHolder)
                Destroy(tf.gameObject);

        }
    }
    public void SpawnCxAgain(Customer cx)
    {
        Tween.Value(0, 1, (val) => { }, Random.Range(8, 20), 0, completeCallback: cx.SpawnCx);
    }

    public void PenalizeAll(Customer customerHandle)
    {
        PlayerId[0].UpdateScore(
    PlayerId[0].ScoreCount - customerHandle.Order.Count
    );
        PlayerId[1].UpdateScore(
    PlayerId[1].ScoreCount - customerHandle.Order.Count
    );
    }

    public void Penalize(int playerId,Customer customerHandle)
    {
        PlayerId[playerId].UpdateScore(
            PlayerId[playerId].ScoreCount - customerHandle.Order.Count
        );
    }

    public void InitReward(string playerTag)
    {
        var go =Instantiate(rewardPf,rewardHolder);
        var comp = go.GetComponent<Reward>();
        comp.RewardFor = playerTag;
        comp.init();

    }
    public void EndGame()
    {
        if(PlayerId[0].IsDead && PlayerId[1].IsDead)
        {
            //dead Screen
            foreach (Player p in PlayerId)
                p.gameObject.SetActive(false);
            gameUI.SetActive(false);
            deadUI.SetActive(true);
        }
    }

    public void SubmitScore()
    {
       

        var score = PlayerId[0].ScoreCount > playerId[1].ScoreCount ? PlayerId[0].ScoreCount : PlayerId[1].ScoreCount;

        bool isScoreSet = false;

        int[] highScores = new int[10];


        for (int i = 0; i < highScores.Length; i++)
        {

            //Get the highScore from 1 - 5
            var highScoreKey = "HighScore" + (i + 1).ToString();
            var scoreStr = PlayerPrefs.GetString(highScoreKey, "0.-");
            var playerInfo = scoreStr.Split('.');

            if (!isScoreSet && score >= int.Parse(playerInfo[0]))
            {

                PlayerPrefs.SetString(highScoreKey, score+"." + winnerName.text);
                isScoreSet = true;
                if (winnerName.text.Length == 0)
                    winnerName.text = "No Name";
                scoreList.text += winnerName.text + "\t" + score + "\n";

            }
            else
                scoreList.text += playerInfo[1] + "\t" + playerInfo[0] + "\n";
        }
        scoreList.gameObject.SetActive(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}



public static class Interactables
{
    public enum Item
    {
        NONE,
        VEGGIE,
        CHOPBOARD_L,
        CHOPBOARD_R,
        HOLDER,
        BIN,
        CUSTOMER,
        PLATE_L,
        PLATE_R
    };
}

